function checkurl(data::CrawlData, url)
    push!(data.urlsvisited, url)

    res = HTTP.get(url; readtimeout=2, redirect=false, status_exception=false, require_ssl_verification=false)
    if res.status == 200
        push!(data.urlsvalid, url)
    elseif HTTP.isredirect(res)
        push!(data.urls3xx, url)

        target = nothing
        for (key, value) in res.headers
            if lowercase(key) == "location"
                target = value
                break
            end
        end
        if target == nothing
            @warn "No redirect target on redirect at $url "
            push!(data.urlsfail, url)
        else
            @info "Checking redirect target $target …"
            checkurl(data, target)
        end
    else
        push!(data.urlsfail, url)
    end
end

function rebase(url, base)
    m = match(r"^(?<protocol>[a-zA-Z0-9]+)\:\/\/(?<host>[^\/]+)\/(?<path>.*)$", url)
    return base * m[:path]
end

function check(base)
    data = CrawlData()
    fnin::Filenames=Filenames()
    fnout = Filenames(
        "new.sitemap.urls.xml",
        "new.sitemap.visited.xml",
        "new.sitemap.valid.xml",
        "new.sitemap.404.xml",
        "new.sitemap.3xx.xml",
        "new.sitemap.external.xml",
        "new.sitemap.fail.xml",
        "new.sitemap.ignored.xml",
    )
    #read(data.urls, fnin.urls)
    read(data.urls, fnin.urlsvalid)

    read(data.urlsvisited, fnout.urlsvisited)
    read(data.urlsvalid, fnout.urlsvalid)
    read(data.urls404, fnout.urls404)
    read(data.urls3xx, fnout.urls3xx)
    read(data.urlsfail, fnout.urlsfail)

    left = length(data.urls)
    limit = 1000
    i = 0
    @info "Checking $(left) URLs, $(length(data.urlsvisited)) already checked …"
    for url in data.urls
        if i > limit
            break
        end
        if left % 100 == 0
            @info "$left left"
        end
        left = left - 1
        if in(url, data.urlsvisited) == false
            i = i + 1
            if length(base) > 0
                @debug "Checking URL $url …"
                url = rebase(url, base)
                checkurl(data, url)
            else
                @debug "Checking URL $url …"
                checkurl(data, url)
            end
        end
    end

    write_crawldata(data, fnout)
end
