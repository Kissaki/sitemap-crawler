using HTTP

include("Sitemap.jl")
include("filenames.jl")
include("crawldata.jl")
include("crawl.jl")
include("check.jl")

function print_usage()
    println("Usage: <action> <param>")
    println("<action>:")
    println("  crawl <base URL>")
    println("  check <base URL>")
    println("For example: crawl https://example.org/")
end

filenames = Filenames()

if length(ARGS) != 2
    @error "Parameters <action> and <param> are required"
    print_usage()
    exit(1)
end
action = ARGS[1]
param = ARGS[2]

try
    if action == "crawl"
        # TODO: Make sure to use trailing slash
        crawl_and_generate(param; forcehttps=true)
    elseif action == "check"
        check(param)
    else
        @error "Unknown action $action"
        print_usage()
        exit(1)
    end
catch ex
    if isa(ex, InterruptException)
        @info "Cancelled via interrupt"
    end
    throw(ex)
end
