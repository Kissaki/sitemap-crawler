function handlehref_abs(data::CrawlData, base, url, href, href_protocol; forcehttps=false)
    if href_protocol == "http" && forcehttps
        href_noprot = chop(href; head=length(href_protocol), tail=0)
        href_protocol = "https"
        href = "https" * href_noprot
    end

    if href_protocol == "http" || href_protocol == "https"
        # Index urls that start with the base URL
        if startswith(href, base)
            push!(data.urls, href)
            @debug "Match (absolute URL): $href"
        else
            @debug "External URL: $href"
            push!(data.urlsexternal, href)
        end
    else
        @debug "Ignoring URL $href with ignored procotol $href_protocol"
    end
end

function handlehref(data::CrawlData, base, url, href; forcehttps=false)
    # Remove URL fragment (#)
    href = match(r"^(?<path>[^\#]*)", href)[:path]

    href_protocol = match(r"^((?<protocol>[a-zA-Z0-9]+)?\:\/\/)?", href)[:protocol]
    if href_protocol != nothing
        handlehref_abs(data, base, url, href, href_protocol; forcehttps=forcehttps)
    elseif startswith(href, "/")
        abshref = base * chop(href; head=1, tail=0)
        @debug "Match (absolute path): $abshref"
        push!(data.urls, abshref)
    else
        abshref = url * href
        @debug "Match (relative path): $abshref"
        push!(data.urls, abshref)
    end
end

function visit(data::CrawlData, url, base; forcehttps=false)
    @info "Visiting $url …"
    res = HTTP.get(url; readtimeout=2, redirect=false, status_exception=false)
    if res.status == 404
        @info "Ignoring HTTP 404 status code url $url"
        push!(data.urls404, url)
    elseif HTTP.isredirect(res) # res.status >= 300 && res.status < 400
        push!(data.urls3xx, url)

        for (name, value) in res.headers
            if name == "Location"
                @debug "Identified redirect $url to $value"
                handlehref(data, base, url, value; forcehttps=forcehttps)
                break
            end
        end
    elseif res.status == 200
        push!(data.urlsvalid, url)

        # Scan for new URLs on this page
        body = String(res.body)
        for m in eachmatch(r"href=\"(?<url>[^\"]+)\"", body)
            href = m[:url]
            handlehref(data, base, url, href; forcehttps=forcehttps)
        end
    else
        @debug "For $url response status is $(res.status)"
        push!(data.urlsfail, url)
    end
    push!(data.urlsvisited, url)
end

function write_urls(filename, urls)
    open(filename, "w") do f
        for url in urls
            println(f, url)
        end
    end
end

function basechop(base)
    if startswith(base, "http://")
        return chop(base; head=length("http://"), tail=0)
    elseif startswith(base, "https://")
        return chop(base; head=length("https://"), tail=0)
    else
        @error "Unexpected prefix "
    end
end

function crawl_and_generate(base; forcehttps=false, limitnew=1000)
    m = match(r"(<?protocol>(http)|(https))\:\/\/[^\/]+(?<port>\:[0-9]+)?(?<trailslash>\/)?(?<path>)?", base)
    if m == nothing
        @error "Failed to parse passed URL"
        exit(1)
    elseif m[:trailslash] == nothing
        @error "Missing trailing slash"
        exit(1)
    end

    urlsignoredprefixes = [
        "https://kcode.de/wordpress/wp-json/",
        "https://kcode.de/wordpress/tag/",
    ]

    data = read_crawldata()

    @info stringlengths(data)

    if forcehttps && startswith(base, "http://")
        base = "https://" * chop(base; head=length("http://", tail=0))
    end

    push!(data.urls, base)
    visited = 0
    while length(data.urlsvisited) != length(data.urls)
        if visited > limitnew
            break
        end

        @info "Intermediate count: $(stringlengths(data)) …"

        for url in data.urls
            if visited > limitnew
                break
            end
            for pref in urlsignoredprefixes
                if startswith(url, pref)
                    push!(data.urlsvisited, url)
                    push!(data.urlsignored, url)
                    continue
                end
            end
            if in(url, data.urlsvisited) == false
                visited = visited + 1
                visit(data, url, base; forcehttps=forcehttps)
            end
        end
    end

    @info "Identified $(stringlengths(data))"

    write_crawldata(data)
end
