struct Filenames
    urls
    urlsvisited
    urlsvalid
    urls404
    urls3xx
    urlsexternal
    urlsfail
    urlsignored

    Filenames() = new(
        "sitemap.urls.xml",
        "sitemap.visited.xml",
        "sitemap.valid.xml",
        "sitemap.404.xml",
        "sitemap.3xx.xml",
        "sitemap.external.xml",
        "sitemap.fail.xml",
        "sitemap.ignored.xml",
    )
    Filenames(
        urls
        , urlsvisited
        , urlsvalid
        , urls404
        , urls3xx
        , urlsexternal
        , urlsfail
        , urlsignored
    ) = new(
        urls
        , urlsvisited
        , urlsvalid
        , urls404
        , urls3xx
        , urlsexternal
        , urlsfail
        , urlsignored
    )
end
